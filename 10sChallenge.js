
 
var mode;                   // ストップウォッチのモード RUN/STOP
var startTime;              // スタートした時刻
var nowTime;                // ストップした時刻
var addTime;                // 経過時間（ストップウォッチ再開時に加算する）
var millisec;                   // 1000分の1秒
var sec100;                 // 100分の1秒
var sec;                        // 秒
var min;                        // 分
var gmt;                        // タイムゾーンのオフセット値
                            //  例）GMT+0900 なら 標準時より9時間後をさしているので-9する
var timerId;                    // タイマー
 
/*
 * 定数
 */
var RUN = 1;                // 動作中
var STOP = 0;               // 停止中
 
/*
 * ストップウォッチのリセット
 */




function resetStopWatch(){
    mode = STOP;    //modeは動いてるかどうかの変数でそこにリセット、つまり止まっているをいれる
    addTime = 0;    //経過時間を保持する変数にも0を
    millisec = sec100 = sec = min　= 0;   //表示されてる時間にすべて0を代入する
    gmt = new Date().getTimezoneOffset() / 60;  // 戻り値は分のため60で割る
    document.getElementById("time").innerHTML = "00:00.00";　//ここはわかる
}
         
/*
 * ボタン処理
 */
function startStop(){
    switch(mode){   //ifでもいけるが対象が１つで様々な条件を比べる時にはswitchの方がいい、可変にも優れている気がする
        case STOP:      // スタートを押したとき
            mode = RUN;
            startTime = new Date().getTime();   //これはスタートをクリックしたときの時間
            document.getElementById("reset").disabled = "true";  // クリアボタンを使用不可
            document.getElementById("button").innerHTML = "ストップ";
            timerId = setTimeout(runStopWatch, 1);     //なぜ10ミリ秒？setTimeoutは一定時間後に()内のfunctionが動く、その一定時間後が,の次に入る、ここでは10になってる、なぜ？　　特に理由はなさそうだから1ミリ秒に変更
            // スタート時刻を設定（ストップウォッチが進んでいれば加算）
            addTime = (min*60*1000 + sec * 1000 + millisec);    //ここのaddTimeはstopを押したときのタイマーの秒数になっている
            startTime -= addTime; /*クリックしたときの時間からタイマーに表示されている秒数を引いたstartTimeをrunStopWatchで使うことでaddTimeを足し算できている
            ex, 08:20,00 startTime
                    15   addTime
                08:35,00 nowTime
                09:00,00 startTime1
                    10   addTime1
                09:10,00 nowTime1
                の場合、startTime1-addTime = 08:45,00になり
                nowTime1の時のaddTimeはaddTime + addTime1の結果を返せる、ひゃっほぉう！
            */
            /*jQuery(function(){
                $("#time").fadeOut("slow"); //ここのjqueryが反応してない
            });*/
            break;
 
        case RUN:       // ストップを押したとき
            mode = STOP;
            clearTimeout(timerId);
//          nowTime = new Date().getTime();
            document.getElementById("button").innerHTML = "スタート";
            document.getElementById("reset").disabled = "";  // クリアボタンを使用可
            //jQuery("#time").fadeIn("fast");     //ここのJqueryも
            drawTime();
    }
}
/*
 * 時間計測
 */
function runStopWatch(){
    // スタートからの差分をとる
    nowTime = new Date().getTime();     //Dateクラスのオブジェクトのミリ秒に変換した値を返す、1232222111...みたいな数値になっている、これは1970年1月1日0時0分0秒(UTC)を起点とした経過時間
    diff = new Date(nowTime - startTime);   
    // ミリ秒、100分の1秒、秒、分、時を設定
    millisec = diff.getMilliseconds();
    sec100 = Math.floor(millisec / 10);
    sec = diff.getSeconds();
    min = diff.getMinutes();
    
    drawTime();         // 時間表示
    timerId = setTimeout(runStopWatch, 1);  //ここがないとタイマー動かない、繰り返しrunStopWatchを動かしてる
}
 
/*
 * 時間表示
 */
function drawTime(){
    var strTime = "";
    var strSec100, strSec, strMin;
 
    // 数値を文字に変換及び2桁表示設定
    strSec100 = "" + sec100;
    if ( strSec100.length < 2){
        strSec100 = "0" + strSec100;
    }
    strSec = "" + sec;
    if ( strSec.length < 2){
        strSec = "0" + strSec;
    }
    strMin = "" + min;
    if ( strMin.length < 2){
        strMin = "0" + strMin;
    }
    
    // 表示形式を設定
    strTime = strMin + ":" + strSec + "." + strSec100;
    document.getElementById("time").innerHTML = strTime;
}
 

 
/*
 * 実行時の処理
 */
window.onload = function(){
    resetStopWatch();
}

